/*Criação e ativação do banco de dados*/
CREATE DATABASE CSJ;

USE CSJ;

/*Criação das tabelas*/
CREATE TABLE tbProdutos (
ID int NOT NULL,
Codigo varchar(20) UNIQUE,
Descricao varchar(100),
Grupo varchar(10),
Preco decimal(8,2),
PRIMARY KEY (ID)
);

CREATE TABLE tbClientes (
CPF varchar(20) NOT NULL,
Nome varchar(10),
Cidade varchar(30),
Bairro varchar(30),
PRIMARY KEY (CPF)
);

CREATE TABLE tbPedidos (
ID int NOT NULL,
Pedido varchar(20),
Data date,
ClienteCPF varchar(20),
PRIMARY KEY (ID),
FOREIGN KEY(ClienteCpf) REFERENCES tbClientes(CPF)
);

CREATE TABLE tbPedidosItens (
PedidoID int NOT NULL,
ProdutoID int NOT NULL,
Qtde int,
Unitario decimal(8,2),
Desconto decimal(8,2),
PRIMARY KEY (PedidoID, ProdutoID),
FOREIGN KEY(PedidoID) REFERENCES tbPedidos(ID),
FOREIGN KEY(ProdutoID) REFERENCES tbProdutos(ID)
);

/*Item 1*/
INSERT INTO tbProdutos (ID, Codigo, Descricao, Grupo, Preco) 
values
(1, 0001, 'Produto Teste 01', 'CELULARES', 30.00),
(2, 0002, 'Produto Teste 02', 'CELULARES', 35.00),
(3, 0003, 'Produto Teste 03', 'CAPAS', 40.00),
(4, 0004, 'Produto Teste 04', 'CAPAS', 45.00),
(5, 0005, 'Produto Teste 05', 'CELULARES', 50.00);

/*Item 2*/
INSERT INTO tbClientes (CPF, Nome, Cidade, Bairro) 
values
('0000001', '0000001', 'São José dos Campos', 'Jardim América');

INSERT INTO tbPedidos (ID, Pedido, Data, ClienteCPF) 
values
(1, '1', '2021-10-10', '0000001');

INSERT INTO tbPedidosItens (PedidoID, ProdutoID, Qtde, Unitario, Desconto) 
values
(1, 1, 10, 30.00, 0.00),
(1, 2, 5, 35.00, 0.00);

/*Item 3*/
INSERT INTO tbClientes (CPF, Nome, Cidade, Bairro) 
values
('000002', '000002', 'São José dos Campos', 'Parque Industrial');

INSERT INTO tbPedidos (ID, Pedido, Data, ClienteCPF) 
values
(2, '2', '2021-07-22', '000002');

INSERT INTO tbPedidosItens (PedidoID, ProdutoID, Qtde, Unitario, Desconto) 
values
(2, 2, 10, 35.00, 0.00),
(2, 3, 10, 40.00, 4.00);

/*Item 4*/
SELECT
  tbClientes.CPF,
  tbClientes.Nome,
  tbClientes.Bairro,
  tbClientes.Cidade,
  tbPedidos.ID,
  tbPedidos.Data,
  tbPedidos.Pedido,
  tbProdutos.Descricao,
  tbPedidosItens.Qtde,
  tbPedidosItens.Unitario,
  tbPedidosItens.Desconto
FROM tbClientes
JOIN tbPedidos
  ON ClienteCPF = tbClientes.CPF
JOIN tbPedidosItens
  ON ID = tbPedidosItens.PedidoID;
JOIN tbProdutos
  ON ProdutoID = tbProdutos.ID;
WHERE tbPedidos.Data>='01/01/2021' and tbPedidos.Data<='31/12/2021';

/*Item 5*/
SELECT
  tbPedidos.Data,
  tbPedidos.Pedido,
  tbClientes.Nome,
  tbClientes.Bairro,
  tbClientes.Cidade,
  tbPedidosItens.Qtde,
  (tbPedidosItens.Unitario*tbPedidosItens.Qtde) AS TOTAL_BRUTO,
  tbPedidosItens.Desconto,
  (TOTAL_BRUTO-tbPedidosItens.Desconto) AS TOTAL_LIQUIDO
FROM tbPedidos
JOIN tbClientes
  ON CPF = tbPedidos.ClienteCPF
JOIN tbPedidosItens
  ON ID = tbPedidosItens.PedidoID;
WHERE tbPedidos.Data>='01/01/2021' and tbPedidos.Data<='31/12/2021';

/*Item 6*/
SELECT
  tbPedidos.Data,
  tbClientes.Nome,
  tbClientes.Cidade,
  tbPedidosItens.Qtde,
  (tbPedidosItens.Unitario*tbPedidosItens.Qtde) AS TOTAL_BRUTO,
  tbPedidosItens.Desconto,
  (TOTAL_BRUTO-tbPedidosItens.Desconto) AS TOTAL_LIQUIDO
FROM tbPedidos
JOIN tbClientes
  ON CPF = tbPedidos.ClienteCPF
WHERE tbPedidos.Data>='01/01/2021' and tbPedidos.Data<='31/12/2021'
GROUP BY tbPedidos.Data;

/*Item 7*/
SELECT
  tbClientes.Nome,
  tbClientes.Cidade,
  tbPedidosItens.Qtde,
  (tbPedidosItens.Unitario*tbPedidosItens.Qtde) AS TOTAL_BRUTO,
  tbPedidosItens.Desconto,
  (TOTAL_BRUTO-tbPedidosItens.Desconto) AS TOTAL_LIQUIDO
FROM tbClientes
JOIN tbPedidos
  ON CPF = tbPedidos.ClienteCPF
JOIN tbPedidosItens
  ON ID = tbPedidosItens.PedidoID

GROUP BY tbClientes.Nome;

/*Item 8*/
SELECT tbClientes.Cidade, tbClientes.Bairro, (tbPedidosItens.Unitario*tbPedidosItens.Qtde) AS TOTAL_BRUTO, tbPedidosItens.Desconto,   (TOTAL_BRUTO-tbPedidosItens.Desconto) AS TOTAL_LIQUIDO, COUNT(*)
FROM tbClientes 
INNER JOIN tbPedidos
  ON CPF = tbPedidos.ClienteCPF
INNER JOIN tbPedidosItens
  ON ID = tbPedidosItens.PedidoID
GROUP BY tbClientes.Cidade
ORDER BY 2 DESC

/*Item 9*/
SELECT tbProdutos.Descricao, tbProdutos.Grupo, tbPedidosItens.Qtde (tbPedidosItens.Unitario*tbPedidosItens.Qtde) AS TOTAL_BRUTO, tbPedidosItens.Desconto, (TOTAL_BRUTO-tbPedidosItens.Desconto) AS TOTAL_LIQUIDO, (TOTAL_BRUTO/tbPedidosItens.Qtde), COUNT(*)
FROM tbProdutos 
INNER JOIN tbPedidosItens
  ON ID = tbPedidosItens.ProdutoID
GROUP BY tbProdutos.Descricao
ORDER BY 2 DESC

/*Item 10*/
DELETE FROM tbClientes WHERE CPF='0000001'

/*Item 11*/
DELETE FROM tbPedidosItens WHERE CPF='0001'