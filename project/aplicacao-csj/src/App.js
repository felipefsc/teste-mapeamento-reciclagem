import logo from './logo.svg';
import './App.css';
import {home} from './home';
import {LocaisReciclagem} from './LocaisReciclagem';
import {BrowserRouter, Route, Switch,NavLink} from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
    <div className="App container">
      <h3 className="d-flex justify-content-center m-3">
        CSJ Sistemas
      </h3>    

      <nav className="navbar navbar-expand-sm bg-light navbar-dark">
        <ul className="navbar-nav">
          <li className="nav-item- m-1">
            <NavLink className="btn btn-light btn-outline-primary" to="/home">
              Mapa
            </NavLink>
          </li>
          <li className="nav-item- m-1">
            <NavLink className="btn btn-light btn-outline-primary" to="/LocaisReciclagem">
              Locais de Reciclagem
            </NavLink>
          </li>
        </ul>
      </nav>

      <Switch>
        <Route path='/home' component={home}/>
        <Route path='/LocaisReciclagem' component={LocaisReciclagem}/>
      </Switch>
    </div>
    </BrowserRouter>
  );
}

export default App;
