import React,{Component} from 'react';
import {variables} from './Variables.js';

export class LocaisReciclagem extends Component{
    constructor(props){
        super(props);

        this.state={
            LocaisReciclagem:[],
            modalTitle:"",
            LocalReciclagem_Id:0,
            Identificacao:"",
            CEP:"",
            Logradouro:"",
            NumeroEndereco:"",
            Complemento:"",
            Bairro:"",
            Cidade:"",
            Capacidade:0,

            LocalReciclagem_IdFilter:"",
            IdentificacaoFilter:"",
            CEPFilter:"",
            LogradouroFilter:"",
            NumeroEnderecoFilter:"",
            ComplementoFilter:"",
            BairroFilter:"",
            CidadeFilter:"",
            CapacidadeFilter:"",
            LocaisReciclagemWithoutFilter:[]
        }
    }

    FilterFn(){
        var LocalReciclagem_IdFilter=this.state.LocalReciclagem_IdFilter;
        var IdentificacaoFilter = this.state.IdentificacaoFilter;
        var CEPFilter=this.state.CEPFilter;
        var LogradouroFilter = this.state.LogradouroFilter;
        var NumeroEnderecoFilter=this.state.NumeroEnderecoFilter;
        var ComplementoFilter = this.state.ComplementoFilter;
        var BairroFilter=this.state.BairroFilter;
        var CidadeFilter = this.state.CidadeFilter;
        var CapacidadeFilter=this.state.CapacidadeFilter;

        var filteredData=this.state.LocaisReciclagemWithoutFilter.filter(
            function(el){
                return el.LocalReciclagem_Id.toString().toLowerCase().includes(
                    LocalReciclagem_IdFilter.toString().trim().toLowerCase()
                )&&
                el.Identificacao.toString().toLowerCase().includes(
                    IdentificacaoFilter.toString().trim().toLowerCase()
                )&&
                el.CEP.toString().toLowerCase().includes(
                    CEPFilter.toString().trim().toLowerCase()
                )&&
                el.Logradouro.toString().toLowerCase().includes(
                    LogradouroFilter.toString().trim().toLowerCase()
                )&&
                el.NumeroEndereco.toString().toLowerCase().includes(
                    NumeroEnderecoFilter.toString().trim().toLowerCase()
                )&&
                el.Complemento.toString().toLowerCase().includes(
                    ComplementoFilter.toString().trim().toLowerCase()
                )&&
                el.Bairro.toString().toLowerCase().includes(
                    BairroFilter.toString().trim().toLowerCase()
                )&&
                el.Cidade.toString().toLowerCase().includes(
                    CidadeFilter.toString().trim().toLowerCase()
                )&&
                el.Capacidade.toString().toLowerCase().includes(
                    CapacidadeFilter.toString().trim().toLowerCase()
                )
            }
        );

        this.setState({LocaisReciclagem:filteredData});

    }

    sortResult(prop,asc){
        var sortedData=this.state.LocaisReciclagemWithoutFilter.sort(function(a,b){
            if(asc){
                return (a[prop]>b[prop])?1:((a[prop]<b[prop])?-1:0);
            }
            else{
                return (b[prop]>a[prop])?1:((b[prop]<a[prop])?-1:0);
            }
        });

        this.setState({LocaisReciclagem:sortedData});
    }

    changeLocalReciclagem_IdFilter = (e)=>{
        this.state.LocalReciclagem_IdFilter=e.target.value;
        this.FilterFn();
    }
    changeIdentificacaoFilter = (e)=>{
        this.state.IdentificacaoFilter=e.target.value;
        this.FilterFn();
    }
    changeCEPFilter = (e)=>{
        this.state.CEPFilter=e.target.value;
        this.FilterFn();
    }
    changeLogradouroFilter = (e)=>{
        this.state.LogradouroFilter=e.target.value;
        this.FilterFn();
    }
    changeNumeroEnderecoFilter = (e)=>{
        this.state.NumeroEnderecoFilter=e.target.value;
        this.FilterFn();
    }
    changeComplementoFilter = (e)=>{
        this.state.ComplementoFilter=e.target.value;
        this.FilterFn();
    }
    changeBairroFilter = (e)=>{
        this.state.BairroFilter=e.target.value;
        this.FilterFn();
    }
    changeCidadeFilter = (e)=>{
        this.state.CidadeFilter=e.target.value;
        this.FilterFn();
    }
    changeCapacidadeFilter = (e)=>{
        this.state.CapacidadeFilter=e.target.value;
        this.FilterFn();
    }

    refreshList(){
        fetch(variables.API_URL+'LocaisReciclagem')
        .then(response=>response.json())
        .then(data=>{
            this.setState({LocaisReciclagem:data,LocaisReciclagemWithoutFilter:data});
        });
    }

    componentDidMount(){
        this.refreshList();
    }
    
    changeIdentificacao =(e)=>{
        this.setState({Identificacao:e.target.value});
    }

    changeCEP =(e)=>{
        this.setState({CEP:e.target.value});
    }

    changeLogradouro =(e)=>{
        this.setState({Logradouro:e.target.value});
    }

    changeNumeroEndereco =(e)=>{
        this.setState({NumeroEndereco:e.target.value});
    }

    changeComplemento =(e)=>{
        this.setState({Complemento:e.target.value});
    }

    changeBairro =(e)=>{
        this.setState({Bairro:e.target.value});
    }

    changeCidade =(e)=>{
        this.setState({Cidade:e.target.value});
    }

    changeCapacidade =(e)=>{
        this.setState({Capacidade:e.target.value});
    }

    addClick(){
        this.setState({
            modalTitle:"Adicionar local de reciclagem",
            LocalReciclagem_Id:0,
            Identificacao:"",
            CEP:"",
            Logradouro:"",
            NumeroEndereco:"",
            Complemento:"",
            Bairro:"",
            Cidade:"",
            Capacidade:0
        });
    }
    editClick(loc){
        this.setState({
            modalTitle:"Alterar dados do local",
            LocalReciclagem_Id:loc.LocalReciclagem_Id,
            Identificacao:loc.Identificacao,
            CEP:loc.CEP,
            Logradouro:loc.Logradouro,
            NumeroEndereco:loc.NumeroEndereco,
            Complemento:loc.Complemento,
            Bairro:loc.Bairro,
            Cidade:loc.Cidade,
            Capacidade:loc.Capacidade
        });
    }

    createClick(){
        fetch(variables.API_URL+'LocaisReciclagem',{
            method:'POST',
            headers:{
                'Accept':'application/json',
                'Content-Type':'application/json'
            },
            body:JSON.stringify({
                LocalReciclagem_Id:this.state.LocalReciclagem_Id,
                Identificacao:this.state.Identificacao,
                CEP:this.state.CEP,
                Logradouro:this.state.Logradouro,
                NumeroEndereco:this.state.NumeroEndereco,
                Complemento:this.state.Complemento,
                Bairro:this.state.Bairro,
                Cidade:this.state.Cidade,
                Capacidade:this.state.Capacidade
            })
        })
        .then(res=>res.json())
        .then((result)=>{
            alert(result);
            this.refreshList();
        },(error)=>{
            alert('Failed');
        })
    }


    updateClick(){
        fetch(variables.API_URL+'LocaisReciclagem',{
            method:'PUT',
            headers:{
                'Accept':'application/json',
                'Content-Type':'application/json'
            },
            body:JSON.stringify({
                LocalReciclagem_Id:this.state.LocalReciclagem_Id,
                Identificacao:this.state.Identificacao,
                CEP:this.state.CEP,
                Logradouro:this.state.Logradouro,
                NumeroEndereco:this.state.NumeroEndereco,
                Complemento:this.state.Complemento,
                Bairro:this.state.Bairro,
                Cidade:this.state.Cidade,
                Capacidade:this.state.Capacidade
            })
        })
        .then(res=>res.json())
        .then((result)=>{
            alert(result);
            this.refreshList();
        },(error)=>{
            alert('Failed');
        })
    }

    deleteClick(LocalReciclagem_Id){
        if(window.confirm('Você tem certeza que gostaria de apagar o registro?')){
        fetch(variables.API_URL+'LocaisReciclagem/'+LocalReciclagem_Id,{
            method:'DELETE',
            headers:{
                'Accept':'application/json',
                'Content-Type':'application/json'
            }
        })
        .then(res=>res.json())
        .then((result)=>{
            alert(result);
            this.refreshList();
        },(error)=>{
            alert('Failed');
        })
        }
    }

    render(){
        const {
            LocaisReciclagem,
            modalTitle,
            LocalReciclagem_Id,
            Identificacao,
            CEP,
            Logradouro,
            NumeroEndereco,
            Complemento,
            Bairro,
            Cidade,
            Capacidade
        }=this.state;

        return(
<div>

    <button type="button"
    className="btn btn-primary m-2 float-end"
    data-bs-toggle="modal"
    data-bs-target="#exampleModal"
    onClick={()=>this.addClick()}>
        Adicionar local de reciclagem
    </button>
    <table className="table table-striped">
    <thead>
    <tr>
        <th>
            <div className="d-flex flex-row">

            
            <input className="form-control m-2"
            onChange={this.changeLocalReciclagem_IdFilter}
            placeholder="Filter"/>
            
            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('LocalReciclagem_Id',true)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-down-square-fill" viewBox="0 0 16 16">
                <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L7.5 10.293V4.5a.5.5 0 0 1 1 0z"/>
                </svg>
            </button>

            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('LocalReciclagem_Id',false)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-up-square-fill" viewBox="0 0 16 16">
                <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2zm6.5-4.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 1 0z"/>
                </svg>
            </button>

            </div>
            LocalReciclagem_Id
        </th>
        <th>
        <div className="d-flex flex-row">
        <input className="form-control m-2"
            onChange={this.changeIdentificacaoFilter}
            placeholder="Filter"/>

            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('Identificacao',true)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-down-square-fill" viewBox="0 0 16 16">
                <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L7.5 10.293V4.5a.5.5 0 0 1 1 0z"/>
                </svg>
            </button>

            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('Identificacao',false)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-up-square-fill" viewBox="0 0 16 16">
                <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2zm6.5-4.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 1 0z"/>
                </svg>
            </button>
            </div>
            Identificacao
      
        </th>
        <th>
            <div className="d-flex flex-row">

            
            <input className="form-control m-2"
            onChange={this.changeCEPFilter}
            placeholder="Filter"/>
            
            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('CEP',true)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-down-square-fill" viewBox="0 0 16 16">
                <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L7.5 10.293V4.5a.5.5 0 0 1 1 0z"/>
                </svg>
            </button>

            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('CEP',false)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-up-square-fill" viewBox="0 0 16 16">
                <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2zm6.5-4.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 1 0z"/>
                </svg>
            </button>

            </div>
            CEP
        </th>
        <th>
            <div className="d-flex flex-row">

            
            <input className="form-control m-2"
            onChange={this.changeLogradouroFilter}
            placeholder="Filter"/>
            
            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('Logradouro',true)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-down-square-fill" viewBox="0 0 16 16">
                <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L7.5 10.293V4.5a.5.5 0 0 1 1 0z"/>
                </svg>
            </button>

            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('Logradouro',false)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-up-square-fill" viewBox="0 0 16 16">
                <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2zm6.5-4.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 1 0z"/>
                </svg>
            </button>

            </div>
            Logradouro
        </th>
        <th>
            <div className="d-flex flex-row">

            
            <input className="form-control m-2"
            onChange={this.changeNumeroEnderecoFilter}
            placeholder="Filter"/>
            
            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('NumeroEndereco',true)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-down-square-fill" viewBox="0 0 16 16">
                <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L7.5 10.293V4.5a.5.5 0 0 1 1 0z"/>
                </svg>
            </button>

            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('NumeroEndereco',false)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-up-square-fill" viewBox="0 0 16 16">
                <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2zm6.5-4.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 1 0z"/>
                </svg>
            </button>

            </div>
            NumeroEndereco
        </th>
        <th>
            <div className="d-flex flex-row">

            
            <input className="form-control m-2"
            onChange={this.changeComplementoFilter}
            placeholder="Filter"/>
            
            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('Complemento',true)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-down-square-fill" viewBox="0 0 16 16">
                <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L7.5 10.293V4.5a.5.5 0 0 1 1 0z"/>
                </svg>
            </button>

            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('Complemento',false)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-up-square-fill" viewBox="0 0 16 16">
                <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2zm6.5-4.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 1 0z"/>
                </svg>
            </button>

            </div>
            Complemento
        </th>
        <th>
            <div className="d-flex flex-row">

            
            <input className="form-control m-2"
            onChange={this.changeBairroFilter}
            placeholder="Filter"/>
            
            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('Bairro',true)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-down-square-fill" viewBox="0 0 16 16">
                <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L7.5 10.293V4.5a.5.5 0 0 1 1 0z"/>
                </svg>
            </button>

            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('Bairro',false)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-up-square-fill" viewBox="0 0 16 16">
                <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2zm6.5-4.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 1 0z"/>
                </svg>
            </button>

            </div>
            Bairro
        </th>
        <th>
            <div className="d-flex flex-row">

            
            <input className="form-control m-2"
            onChange={this.changeCidadeFilter}
            placeholder="Filter"/>
            
            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('Cidade',true)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-down-square-fill" viewBox="0 0 16 16">
                <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L7.5 10.293V4.5a.5.5 0 0 1 1 0z"/>
                </svg>
            </button>

            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('Cidade',false)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-up-square-fill" viewBox="0 0 16 16">
                <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2zm6.5-4.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 1 0z"/>
                </svg>
            </button>

            </div>
            Cidade
        </th>
        <th>
            <div className="d-flex flex-row">

            
            <input className="form-control m-2"
            onChange={this.changeCapacidadeFilter}
            placeholder="Filter"/>
            
            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('Capacidade',true)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-down-square-fill" viewBox="0 0 16 16">
                <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L7.5 10.293V4.5a.5.5 0 0 1 1 0z"/>
                </svg>
            </button>

            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('Capacidade',false)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-up-square-fill" viewBox="0 0 16 16">
                <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2zm6.5-4.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 1 0z"/>
                </svg>
            </button>

            </div>
            Capacidade
        </th>
        <th>
            Options
        </th>
    </tr>
    </thead>
    <tbody>
        {LocaisReciclagem.map(loc=>
            <tr key={loc.LocalReciclagem_Id}>
                <td>{loc.LocalReciclagem_Id}</td>
                <td>{loc.Identificacao}</td>
                <td>{loc.CEP}</td>
                <td>{loc.Logradouro}</td>
                <td>{loc.NumeroEndereco}</td>
                <td>{loc.Complemento}</td>
                <td>{loc.Bairro}</td>
                <td>{loc.Cidade}</td>
                <td>{loc.Capacidade}</td>
                <td>
                <button type="button"
                className="btn btn-light mr-1"
                data-bs-toggle="modal"
                data-bs-target="#exampleModal"
                onClick={()=>this.editClick(loc)}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-square" viewBox="0 0 16 16">
                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                    <path fillRule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                    </svg>
                </button>

                <button type="button"
                className="btn btn-light mr-1"
                onClick={()=>this.deleteClick(loc.LocalReciclagem_Id)}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash-fill" viewBox="0 0 16 16">
                    <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                    </svg>
                </button>

                </td>
            </tr>
            )}
    </tbody>
    </table>

<div className="modal fade" id="exampleModal" tabIndex="-1" aria-hidden="true">
<div className="modal-dialog modal-lg modal-dialog-centered">
<div className="modal-content">
   <div className="modal-header">
       <h5 className="modal-title">{modalTitle}</h5>
       <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"
       ></button>
   </div>

   <div className="modal-body">
       <div className="input-group mb-3">
        <span className="input-group-text">Identificacao</span>
        <input type="text" className="form-control"
        value={Identificacao}
        onChange={this.changeIdentificacao}/>
       </div>

       <div className="input-group mb-3">
        <span className="input-group-text">CEP</span>
        <input type="text" className="form-control"
        value={CEP}
        onChange={this.changeCEP}/>
       </div>

       <div className="input-group mb-3">
        <span className="input-group-text">Logradouro</span>
        <input type="text" className="form-control"
        value={Logradouro}
        onChange={this.changeLogradouro}/>
       </div>

       <div className="input-group mb-3">
        <span className="input-group-text">NumeroEndereco</span>
        <input type="text" className="form-control"
        value={NumeroEndereco}
        onChange={this.changeNumeroEndereco}/>
       </div>

       <div className="input-group mb-3">
        <span className="input-group-text">Complemento</span>
        <input type="text" className="form-control"
        value={Complemento}
        onChange={this.changeComplemento}/>
       </div>

       <div className="input-group mb-3">
        <span className="input-group-text">Bairro</span>
        <input type="text" className="form-control"
        value={Bairro}
        onChange={this.changeBairro}/>
       </div>

       <div className="input-group mb-3">
        <span className="input-group-text">Cidade</span>
        <input type="text" className="form-control"
        value={Cidade}
        onChange={this.changeCidade}/>
       </div>

       <div className="input-group mb-3">
        <span className="input-group-text">Capacidade</span>
        <input type="text" className="form-control"
        value={Capacidade}
        onChange={this.changeCapacidade}/>
       </div>

        {LocalReciclagem_Id==0?
        <button type="button"
        className="btn btn-primary float-start"
        onClick={()=>this.createClick()}
        >Criar</button>
        :null}

        {LocalReciclagem_Id!=0?
        <button type="button"
        className="btn btn-primary float-start"
        onClick={()=>this.updateClick()}
        >Atualizar</button>
        :null}

   </div>

</div>
</div> 
</div>


</div>
        )
    }
}