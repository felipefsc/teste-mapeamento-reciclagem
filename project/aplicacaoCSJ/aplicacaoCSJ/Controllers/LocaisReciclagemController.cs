﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using aplicacaoCSJ.Models;

namespace aplicacaoCSJ.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocaisReciclagemController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public LocaisReciclagemController(IConfiguration configuration)
        {
            _configuration = configuration;
        }


        [HttpGet]
        public JsonResult Get()
        {
            string query = @"select LocalReciclagem_Id, Identificacao, CEP, Logradouro, NumeroEndereco, Complemento, Bairro, Cidade, Capacidade from LocaisReciclagem";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("aplicacaoCSJ");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }

        [HttpPost]
        public JsonResult Post(LocaisReciclagem loc)
        {
            string query = @"insert into LocaisReciclagem values (@LocalReciclagem_Id,@Identificacao,@CEP,@Logradouro,@NumeroEndereco,@Complemento,@Bairro,@Cidade,@Capacidade)";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("aplicacaoCSJ");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@LocalReciclagem_Id", loc.LocalReciclagem_Id);
                    myCommand.Parameters.AddWithValue("@Identificacao", loc.Identificacao);
                    myCommand.Parameters.AddWithValue("@CEP", loc.CEP);
                    myCommand.Parameters.AddWithValue("@Logradouro", loc.Logradouro);
                    myCommand.Parameters.AddWithValue("@NumeroEndereco", loc.NumeroEndereco);
                    myCommand.Parameters.AddWithValue("@Complemento", loc.Complemento);
                    myCommand.Parameters.AddWithValue("@Bairro", loc.Bairro);
                    myCommand.Parameters.AddWithValue("@Cidade", loc.Cidade);
                    myCommand.Parameters.AddWithValue("@Capacidade", loc.Capacidade);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Campo adicionado com sucesso!");
        }

        [HttpPut]
        public JsonResult Put(LocaisReciclagem loc)
        {
            string query = @"update LocaisReciclagem set Identificacao= @Identificacao, CEP= @CEP, Logradouro= @Logradouro, NumeroEndereco= @NumeroEndereco, Complemento= @Complemento, Bairro= @Bairro, Cidade= @Cidade, Capacidade= @Capacidade where LocalReciclagem_Id=@LocalReciclagem_Id";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("aplicacaoCSJ");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@LocalReciclagem_Id", loc.LocalReciclagem_Id);
                    myCommand.Parameters.AddWithValue("@Identificacao", loc.Identificacao);
                    myCommand.Parameters.AddWithValue("@CEP", loc.CEP);
                    myCommand.Parameters.AddWithValue("@Logradouro", loc.Logradouro);
                    myCommand.Parameters.AddWithValue("@NumeroEndereco", loc.NumeroEndereco);
                    myCommand.Parameters.AddWithValue("@Complemento", loc.Complemento);
                    myCommand.Parameters.AddWithValue("@Bairro", loc.Bairro);
                    myCommand.Parameters.AddWithValue("@Cidade", loc.Cidade);
                    myCommand.Parameters.AddWithValue("@Capacidade", loc.Capacidade);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Campo atualizado com sucesso!");
        }

        [HttpDelete("{LocalReciclagem_Id}")]
        public JsonResult Delete(int LocalReciclagem_Id)
        {
            string query = @"delete from LocaisReciclagem where LocalReciclagem_Id=@LocalReciclagem_Id";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("aplicacaoCSJ");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@LocalReciclagem_Id", LocalReciclagem_Id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Campo removido com sucesso!");
        }

    }
}
